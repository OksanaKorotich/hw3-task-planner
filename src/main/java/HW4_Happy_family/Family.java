package HW4_Happy_family;

import java.util.*;
import java.util.stream.Collectors;
import java.io.Serializable;

public class Family implements Serializable{
    private final Human mother;
    private final Human father;
    private List<Human> children;
    private Set<Pet> pets;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new ArrayList<>();
        this.pets = new HashSet<>();
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public List<Human> getChildren(){
        return children;
    }


    public  void addPet(Pet pet){
        pets.add(pet);
    }

    public Set<Pet> getPets() {
        return  pets;
    }

    public void addChild(Human child){
        child.setFamily(this);
        children.add(child);
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index >= children.size()) {
            return false;
        }
        children.remove(index);
        return true;
    };

    public boolean deleteChild(Human child) {
        return children.remove(child);
    }

    public int countFamily() {
        int numberOfParents = 2;
        return children.size() + numberOfParents;
    }

    public boolean removePet(Pet pet) {
        return pets.remove(pet);
    }

    public void setPet(Pet pet) {
    }


    public String prettyFormat() {
        String motherInfo = "mother: " + mother.prettyFormat();
        String fatherInfo = "father: " + father.prettyFormat();
        String childrenInfo = children.isEmpty() ? "children: null" : getChildrenInfo();
        String petsInfo = pets.isEmpty() ? "pets: null" : getPetsInfo();

        return String.format("family:%n   %s,%n   %s,%n   %s,%n   %s", motherInfo, fatherInfo, childrenInfo, petsInfo);
    }

    private String getChildrenInfo() {
        StringBuilder childrenInfo = new StringBuilder("children: \n");
        for (Human child : children) {
            childrenInfo.append("   ").append(child.prettyFormat()).append("\n");
        }
        return childrenInfo.toString();
    }

    @Override
    public String toString() {
        String motherInfo = "Mother: " + mother.getName();
        String fatherInfo = "Father: " + father.getName();
        String childrenInfo = children.isEmpty() ? "No children" : getChildrenInfo();
        String petsInfo = pets.isEmpty() ? "No pets" : getPetsInfo();

        return String.format("Family{%s, %s, %s, %s}", motherInfo, fatherInfo, childrenInfo, petsInfo);

    }


    private String getPetsInfo() {
        return "Pets: " + pets.stream()
                .map(Pet::toString)
                .collect(Collectors.joining(", "));
    }

    private String getChildrenNames() {
        StringBuilder names = new StringBuilder();
        for (int i = 0; i < children.size(); i++) {
            if (i > 0) {
                names.append(", ");
            }
            names.append(children.get(i).getName());
        }
        return names.toString();
    }

    private String findPetInfo() {
        if (mother.getPet() != null) {
            return mother.getPet().toString();
        } else if (father.getPet() != null) {
            return father.getPet().toString();
        } else {
            for (Human child : children) {
                if (child.getPet() != null) {
                    return child.getPet().toString();
                }
            }
        }
        return "No pet";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(father, family.father) &&
                Objects.equals(mother, family.mother);
    }

    @Override
    public int hashCode() {
        return Objects.hash(father, mother);
    }


}
