package HW4_Happy_family;

public class Fish extends Pet {
    public Fish(String nickname) {
        super(nickname);
        this.species = Species.FISH;

    }

    @Override
    public void respond() {
        System.out.printf("\nПривіт, я рибка. Мене звати %s!", getNickname());
    }
}
