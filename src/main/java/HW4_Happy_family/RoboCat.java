package HW4_Happy_family;

public class RoboCat extends Pet{

        public RoboCat(String nickname) {
            super(nickname);
            this.species = Species.ROBO_CAT;
        }
        @Override
        public void respond() {
            System.out.printf("\nПривіт! Я - %s, мене звати %s!", getSpecies().getTranslation(), getNickname());
        }
}
