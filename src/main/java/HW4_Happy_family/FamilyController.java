package HW4_Happy_family;

import java.util.List;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int number) {
        return familyService.getFamiliesBiggerThan(number);
    }

    public List<Family> getFamiliesLessThan(int number) {
        return familyService.getFamiliesLessThan(number);
    }

    public long countFamiliesWithMemberNumber(int number) {
        return familyService.countFamiliesWithMemberNumber(number);
    }

    public void createNewFamily(Human father, Human mother) {
        familyService.createNewFamily(father, mother);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        try {
            return familyService.bornChild(family, boyName, girlName);
        } catch (FamilyOverflowException e) {
            System.out.println("Помилка: розмір сім'ї перевищує бажану кількість осіб");
            return null;
        }
    }

    public Family adoptChild(Family family, Human child) {
        try {
            return familyService.adoptChild(family, child);
        } catch (FamilyOverflowException e) {
            System.out.println("Помилка: розмір сім'ї перевищує бажану кількість осіб");
            return null;
        }
    }

    public void deleteAllChildrenOlderThan(int age) {
        familyService.deleteAllChildrenOlderThan(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public List<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet) {
        familyService.addPet(index, pet);
    }

    public void deleteChild(int i, Human daughter) {
    }

    public void createNewFamily(Family newFamily) {
    }

    public void replaceFamilies(List<Family> loadedFamilies) {
    }
}

