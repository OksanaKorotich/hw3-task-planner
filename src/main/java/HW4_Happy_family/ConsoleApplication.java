package HW4_Happy_family;

import java.util.*;

public class ConsoleApplication {


    public static void main(String[] args) {

        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);
        createTestFamilies(familyService, familyController, familyDao );

        Scanner scanner = new Scanner(System.in);
        int choice;
        boolean menuDisplayed = false;
        int countBigger;
        int countLess;
        final int DESIRED_FAMILY_SIZE = 4;

        while (true) {
            if (!menuDisplayed) {
                displayMenu();
                menuDisplayed = true;
            }
            choice = getUserChoice(scanner);

            try {
                switch (choice) {
                    case 1:
                        String saveFilePath = "familiesDB.ser";
                        familyService.saveData(familyController.getAllFamilies(), saveFilePath);
                        System.out.println("Дані збережено успішно.");
                        break;
                    case 2:
                        String loadFilePath = "familiesDB.ser";
                        List<Family> loadedFamilies = familyService.loadData(loadFilePath);
                        familyController.replaceFamilies(loadedFamilies);
                        System.out.println("Дані завантажено успішно.");
                        break;
                    case 3:
                        familyController.displayAllFamilies();
                        break;
                    case 4:
                        int familyCount = familyController.count();
                        System.out.println("Всього сімей: " + familyCount);
                        break;
                    case 5:
                        System.out.println("Введіть кількість членів сім'ї для фільтрації:");
                        int number = scanner.nextInt();
                        scanner.nextLine();
                        countBigger = familyController.getFamiliesBiggerThan(number).size();
                        System.out.println("Кількість сімей, де більше ніж " + number + " чоловік: " + countBigger);
                        break;
                    case 6:
                        System.out.println("Введіть кількість членів сім'ї для фільтрації:");
                        int number1 = scanner.nextInt();
                        scanner.nextLine();
                        countLess = familyController.getFamiliesLessThan(number1).size();
                        System.out.println("Кількість сімей, де менше ніж " + number1 + " чоловік: " + countLess);
                        break;
                    case 7:
                        System.out.println("Введіть кількість членів сім'ї для підрахунку:");
                        int number2 = scanner.nextInt();
                        scanner.nextLine();
                        long count = familyController.countFamiliesWithMemberNumber(number2);
                        System.out.println("Кількість сімей, де " + number2 + " чоловік: " + count);
                        break;
                    case 8:
                        Family newFamily = createFamilyFromUserInput(scanner, familyService);
                        familyController.createNewFamily(newFamily);
                        System.out.println("Нова родина була створена та додана до бази даних.");
                        break;
                    case 9:
                        System.out.println("Введіть індекс сім'ї для видалення:");
                        int familyIndex = scanner.nextInt();
                        scanner.nextLine();
                        familyController.deleteFamilyByIndex(familyIndex);
                        break;
                    case 10:
                        System.out.println("Введіть індекс сім'ї для редагування: ");
                        int indexFamily = scanner.nextInt();
                        scanner.nextLine();

                        if (indexFamily >= 0 && indexFamily < familyController.count()) {
                            Family family = familyController.getFamilyById(indexFamily);
                            System.out.println("Оберіть опцію для редагування:");
                            System.out.println("1. Народити дитину");
                            System.out.println("2. Усиновити дитину");
                            System.out.println("3. Повернутися до головного меню");
                            int editChoice = getUserChoice(scanner);

                            switch (editChoice) {
                                case 1:
                                    if (family.countFamily() >= DESIRED_FAMILY_SIZE) {
                                        throw new FamilyOverflowException("Сім'я вже має максимальну кількість членів.");
                                    }
                                    System.out.println("Введіть ім'я хлопчика: ");
                                    String boyName = scanner.nextLine();
                                    System.out.println("Введіть ім'я дівчинки: ");
                                    String girlName = scanner.nextLine();
                                    familyController.bornChild(family, boyName, girlName);
                                    System.out.println("Дитина народилася.");
                                    break;
                                case 2:
                                    if (family.countFamily() >= DESIRED_FAMILY_SIZE) {
                                        throw new FamilyOverflowException("Сім'я вже має максимальну кількість членів.");
                                    }
                                    System.out.println("Введіть ім'я дитини: ");
                                    String childName = scanner.nextLine();
                                    System.out.println("Введіть прізвіще дитини: ");
                                    String childLastName = scanner.nextLine();
                                    System.out.println("Введіть дату народження дитини (у форматі dd/MM/yyyy): ");
                                    String childBirthDate = scanner.nextLine();
                                    System.out.println("Введіть рівень інтелекту дитини: ");
                                    int childIq = Integer.parseInt(scanner.nextLine());
                                    Human child = new Human(childName, childLastName, childBirthDate, childIq);
                                    familyController.adoptChild(family, child);
                                    System.out.println("Дитина усиновлена.");
                                    break;
                                case 3:
                                    System.out.println("Повернення до головного меню.");
                                    break;
                                default:
                                    System.out.println("Невірний вибір.");
                                    break;
                            }
                        } else {
                            System.out.println("Сім'ю за вказаним індексом не знайдено.");
                        }
                        break;

                    case 11:
                        System.out.println("Введіть вік дітей для видалення: ");
                        int age = scanner.nextInt();
                        scanner.nextLine();
                        familyController.deleteAllChildrenOlderThan(age);
                        System.out.println("Дітей старше " + age + " років видалено.");
                        break;
                    case 12:
                        System.out.println("Програма завершена.");
                        return;
                    default:
                        System.out.println("Невідомий вибір. Спробуйте ще раз.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Помилка: некоректний формат числа.");
            } catch (Exception e) {
                System.out.println("Сталася помилка: " + e.getMessage());
            }
        }
    }

    private static void displayMenu() {
        System.out.println("Меню:");
        System.out.println("1. Зберегти дані");
        System.out.println("2. Завантажити дані");
        System.out.println("3. Відобразити всі сім'ї");
        System.out.println("4. Підрахувати кількість сімей");
        System.out.println("5. Сім'ї з більше ніж X членів");
        System.out.println("6. Сім'ї з менше ніж X членів");
        System.out.println("7. Підрахувати кількість сімей з X членами");
        System.out.println("8. Створити нову родину");
        System.out.println("9. Видалити сім'ю за індексом сім'ї у загальному списку");
        System.out.println("10. Редагувати сім'ю за індексом сім'ї у загальному списку");
        System.out.println("11. Видалити дітей старше ніж X років");
        System.out.println("12. Вийти (exit)");

    }

    private static int getUserChoice(Scanner scanner) {
        int choice;
        while (true) {
            System.out.print("Введіть номер команди: ");
            try {
                choice = Integer.parseInt(scanner.nextLine());
                return choice;
            } catch (NumberFormatException e) {
                System.out.println("Невірний ввід. Введіть число.");
            }
        }
    }

    private static Family createFamilyFromUserInput(Scanner scanner, FamilyService familyService) {
        System.out.println("Створення нової родини:");
        System.out.print("Введіть ім'я матері: ");
        String motherFirstName = scanner.nextLine();
        System.out.print("Введіть прізвище матері: ");
        String motherLastName = scanner.nextLine();
        System.out.print("Введіть дату народження матері (у форматі dd/MM/yyyy): ");
        String motherBirthDate = scanner.nextLine();
        System.out.print("Введіть IQ матері: ");
        int motherIq = scanner.nextInt();
        scanner.nextLine();
        Woman mother = new Woman(motherFirstName, motherLastName, motherBirthDate, motherIq);

        System.out.print("Введіть ім'я батька: ");
        String fatherFirstName = scanner.nextLine();
        System.out.print("Введіть прізвище батька: ");
        String fatherLastName = scanner.nextLine();
        System.out.print("Введіть дату народження батька (у форматі dd/MM/yyyy): ");
        String fatherBirthDate = scanner.nextLine();
        System.out.print("Введіть IQ батька: ");
        int fatherIq = scanner.nextInt();
        scanner.nextLine();
        Man father = new Man(fatherFirstName, fatherLastName, fatherBirthDate, fatherIq);
        return familyService.createNewFamily(mother, father);
    }
    private static void createTestFamilies(FamilyService familyService, FamilyController familyController, FamilyDao familyDao) {
        // Створюємо тестові сім'ї та додаємо їх до бази даних
        Woman mother1 = new Woman("Jane", "Doe", "08/07/1973");
        Man father1 = new Man("John", "Doe", "14/12/1978");
        Family family1 = familyService.createNewFamily(mother1, father1);
        Set<String> habits2 = new HashSet<>(Arrays.asList("bark", "jump", "run"));
        Set<String> habits3 = new HashSet<>(Arrays.asList("їсти", "грати з м'ячем"));
        Dog pet2 = new Dog("Bobik", 5, 88, habits2);
        DomesticCat pet3 = new DomesticCat("Віскерс", 3, 60, habits3);
        Fish pet4 = new Fish("Немо");
        familyController.addPet(0, pet2);
        familyController.addPet(0,pet3);
        familyController.addPet(0,pet4);

        Woman mother2 = new Woman("Дейнеріс", "Сноу", "21/04/1985");
        Man father2 = new Man("Джон", "Сноу", "15/09/1980");
        Family family2 = familyService.createNewFamily(mother2, father2);
        List<Family> families = familyService.getAllFamilies();
        familyService.saveData(families, "familiesDB.ser");
    }


}


