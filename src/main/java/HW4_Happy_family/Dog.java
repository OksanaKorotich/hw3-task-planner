package HW4_Happy_family;

import java.util.Set;

public class Dog extends Pet implements Fouling{
    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.DOG;
    }
    @Override
    public void respond() {
        System.out.printf("\nМнняяав! Я - %s, мене звати %s!", getSpecies().getTranslation(), getNickname());
    }

    public void foul(){
        System.out.println("\nПотрібно добре замести сліди...");
    }
}
