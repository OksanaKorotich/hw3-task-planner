package HW4_Happy_family;

public enum Species {
    DOG("Собака", false, 4, true),
    DOMESTIC_CAT("Домашній кіт", false, 4, true),
    FISH("Рибка", false, 0, false),
    BIRD("Пташка", true, 2, false),
    DRAGON("Дракон", true, 4, false),
    ROBO_CAT("Кіт-робот", false, 4, true),
    UNKNOWN("Невідомий", false, 0,false);


    private final String translation;
    private final boolean canFly;
    private final int numberOfLegs;
    private final boolean hasFur;

    Species(String translation, boolean canFly, int numberOfLegs, boolean hasFur) {
        this.translation = translation;
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

    public String getTranslation() {
        return translation;
    }

    public boolean canFly() {
        return canFly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public boolean hasFur() {
        return hasFur;
    }
}
