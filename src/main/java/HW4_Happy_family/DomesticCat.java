package HW4_Happy_family;

import java.util.Set;

public class DomesticCat extends Pet implements Fouling{
    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.DOMESTIC_CAT;
    }
    @Override
    public void respond() {
        System.out.printf("\nМняяяв! Я - %s, мене звати %s!", getSpecies().getTranslation(), getNickname());
    }

    public void foul(){
        System.out.println("\nПотрібно добре замести сліди...");
    }

}
