package HW4_Happy_family;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.io.Serializable;

public abstract class Pet implements Serializable{
    Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits;
    private Family family;


    public Pet(String nickname) {
        this.nickname = nickname;
    }
    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public static Pet unknownPet(String nickname) {
        return new Pet(nickname) {
            {
                this.species = Species.UNKNOWN;
            }

            @Override
            public void respond() {
                System.out.println("\nНе відомо, як відповідати.");
            }
        };
    }

    // geters, seters

    public String getNickname() {
        return nickname;
    }
    public Species getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }
    public String getTrickLevel() {
        return trickLevel > 50 ? "дуже хитрий" : "майже не хитрий";
    }


    //methods
    public void eat() {
        System.out.println("\nЯ їм!");
    }
    public abstract void respond();

    public String prettyFormat() {
        String habitsInfo = habits != null ? habits.toString() : "null";
        return String.format("{species=%s, nickname='%s', age=%d, trickLevel=%d, habits=%s}",
                species, nickname, age, trickLevel, habitsInfo);
    }

    @Override
    public String toString() {
        return String.format("\n%s{nickname='%s', age=%d, tricklevel=%d, habits=%s, canFly=%b, numberOfLegs=%d, hasFur=%b}",
                species.getTranslation(), nickname, age, trickLevel, habits, species.canFly(), species.getNumberOfLegs(), species.hasFur());
    }


    @Override
    public boolean equals(Object j) {
        if (this == j) return true;
        if (j == null || getClass() != j.getClass()) return false;
        Pet pet = (Pet) j;
        return age == pet.age &&
                Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age);
    }


}
