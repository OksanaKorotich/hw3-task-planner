package HW4_Happy_family;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Woman extends Human {
    private Map<DayOfWeek, String> schedule;
    private int iq;

    public Woman(String name, String surname, String birthDateStr){
        super(name, surname, birthDateStr);
    };
    public Woman(String name, String surname, String birthDateStr, Map<DayOfWeek, String> schedule){
        super(name, surname, birthDateStr);
        this.schedule = new HashMap<>();
    }
    public Woman(String name, String surname, String birthDate, int iq) {
        super(name, surname, birthDate);
        this.iq = iq;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    @Override
    public void greetPet() {
//        Pet pet = getFamily().getPet();

        for(Pet pet: getFamily().getPets()){
            System.out.printf("\nПривіт, %s, я принесла тобі поїсти!", pet.getNickname());
        }
    }

    public void makeup() {
        System.out.println("\nРоблю макіяж.");
    }
}
