package HW4_Happy_family;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class FamilyService {
    private FamilyDao familyDao;
    public FamilyService(FamilyDao familyDao){
        this.familyDao = familyDao;
    }
    public void loadData(List<Family> families) {
        familyDao.loadData(families);
    }

    // Завантаження даних у файл
    public void saveData(List<Family> families, String fileName) {
        String directory = System.getProperty("user.dir");
        String filePath = directory + File.separator + fileName;

        try (FileOutputStream fileOutputStream = new FileOutputStream(filePath);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(families);
            System.out.println("Дані збережено успішно у файл " + filePath);
        } catch (IOException e) {
            System.err.println("Помилка при збереженні даних у файл: " + e.getMessage());
        }
    }

    // Зчитуваня даних
    public List<Family> loadData(String filePath) {
        try (FileInputStream fileInputStream = new FileInputStream(filePath);
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            return (List<Family>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Помилка при завантаженні даних із файлу: " + e.getMessage());
            return new ArrayList<>(); // Повернення пустого списку у разі помилки
        }
    }

    // одержати список всіх сімей.
    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayFamily(Family family) {
        System.out.println(family.prettyFormat());
    }
// вивести на екран усі сім'ї (в індексованому списку) з усіма членами сім'ї


    public void displayAllFamilies() {
        List<Family> allFamilies = familyDao.getAllFamilies();
        if (allFamilies.isEmpty()) {
            System.out.println("Немає збережених сімей.");
        } else {
            for (int i = 0; i < allFamilies.size(); i++) {
                System.out.println("Сім'я №" + i + ":");
                System.out.println(allFamilies.get(i).prettyFormat());
                System.out.println();
            }
        }
    }
    // знайти сім'ї з кількістю людей більше ніж (приймає кількість осіб
    // та повертає всі сім'ї де кількість людей більша за вказану); виводить інформацію на екран.

    public List<Family> getFamiliesBiggerThan(int number) {
        return familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() > number)
                .toList();
    }

    //знайти сім'ї з кількістю людей менше ніж (приймає кількість осіб
    // та повертає всі сім'ї де кількість людей менша ніж зазначена); виводить інформацію на екран.
    public List<Family> getFamiliesLessThan(int number) {
        return familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() < number)
                .toList();
    }

    //підрахувати число сімей з кількістю людей, що дорівнює переданому числу.

    public long countFamiliesWithMemberNumber(int number) {
        return familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() == number)
                .count();
    }

    //створити нову сім'ю (приймає 2 параметри типу Human) - створює нову сім'ю, зберігає у БД.

    public Family createNewFamily(Human father, Human mother) {
        Family family = new Family(father, mother);
        familyDao.saveFamily(family);
        return family;
    }

    // видалити сім'ю за індексом у списку - видаляє сім'ю із БД.

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    //народити сім'єю дитину


    public Family bornChild(Family family, String boyName, String girlName) {
        Human child;
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = dateFormat.format(date);

        if (Math.random() < 0.5) {
            child = new Human(boyName, family.getFather().getSurname(), formattedDate );
        } else {
            child = new Human(girlName, family.getFather().getSurname(), formattedDate);
        }
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }
    //усиновити дитину

    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }

    //видалити дітей старше ніж

    public void deleteAllChildrenOlderThan(int age) {
        familyDao.getAllFamilies()
                .forEach(family -> family.getChildren()
                        .removeIf(child -> calculateAge(child.getBirthDate()) > age));
    }

    private int calculateAge(long birthDate) {
        long currentTime = System.currentTimeMillis();
        long ageInMillis = currentTime - birthDate;
        int days = (int) (ageInMillis / (24 * 60 * 60 * 1000));
        return days / 365;
    }


    //кількість сімей у БД.

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    // приймає індекс сім'ї, повертає Family за вказаним індексом.

    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    //приймає індекс сім'ї, повертає список свійських тварин, які живуть у сім'ї.

    public List<Pet> getPets(int familyIndex) {
        List<Family> families = familyDao.getAllFamilies();
        if (familyIndex >= 0 && familyIndex < families.size()) {
            Set<Pet> petSet = families.get(familyIndex).getPets();
            //System.out.println(families.get(familyIndex));
            return new ArrayList<>(petSet);
        }
        return Collections.emptyList();
    }

//приймає індекс сім'ї та параметр Pet - додає нового вихованця в сім'ю, оновлює дані в БД.

    public void addPet(int familyIndex, Pet pet) {
        List<Family> families = familyDao.getAllFamilies();
        if (familyIndex >= 0 && familyIndex < families.size()) {
            Family family = families.get(familyIndex);
            family.addPet(pet);
            familyDao.saveFamily(family);
        }
    }


}




