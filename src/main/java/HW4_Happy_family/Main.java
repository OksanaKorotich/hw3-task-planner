package HW4_Happy_family;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.security.spec.RSAOtherPrimeInfo;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);
        List<Family> families = new ArrayList<>();

        // family 1
        Map<DayOfWeek, String> schedule1 = new HashMap<>();
        schedule1.put(DayOfWeek.MONDAY, "прибирати");
        schedule1.put(DayOfWeek.THURSDAY, "мити вікна");
        schedule1.put(DayOfWeek.WEDNESDAY, "йти за покупками");
        Woman mother1 = new Woman("Дейнеріс", "Сноу", "21/04/1985", schedule1);
        Man father1 = new Man("Джон", "Сноу", "15/09/1980");
        familyController.createNewFamily(mother1, father1);
        familyController.bornChild(familyDao.getFamilyByIndex(0), "Віктор", "Кейт");
        Family family1 = familyDao.getFamilyByIndex(0); // Отримуємо сім'ю за індексом
        Human son1 = new Human("Алекс", "Сноу", "07/03/1998", 115);
        Human daughter1 = new Human("Лея", "Сноу", "02/11/2007", 110);
        Set<String> habits1 = new HashSet<>(Arrays.asList("спати", "їсти", "дерти шпалери"));
        DomesticCat pet1 = new DomesticCat("Дрогон", 3, 75, habits1);
        familyController.addPet(0, pet1);
        familyController.adoptChild(family1, son1);
        familyController.adoptChild(family1, daughter1);
        mother1.describeAge();
        father1.describeAge();
        son1.describeAge();
        daughter1.describeAge();
        families.add(family1);

        // family 2
        Woman mother2 = new Woman("Jane", "Doe", "08/07/1973");
        Man father2 = new Man("John", "Doe", "14/12/1978");
        familyController.createNewFamily(mother2, father2);
        familyController.bornChild(familyDao.getFamilyByIndex(1), "Junior", "Ariel");
        Set<String> habits2 = new HashSet<>(Arrays.asList("bark", "jump", "run"));
        Dog pet2 = new Dog("Bobik", 5, 88, habits2);
        familyController.addPet(1, pet2);
        Family family2 = familyDao.getFamilyByIndex(1);
        families.add(family2);

        // family 3
        Woman mother3 = new Woman("Ольга", "Петренко", "30/05/1990");
        Man father3 = new Man("Алексей", "Петренко", "18/09/1988");
        familyController.createNewFamily(mother3, father3);
        Human child3 = new Human("Ірина", "Петренко", "12/08/2007", mother3, father3);
        Set<String> habits3 = new HashSet<>(Arrays.asList("їсти", "грати з м'ячем"));
        DomesticCat pet3 = new DomesticCat("Віскерс", 3, 60, habits3);
        Fish pet4 = new Fish("Немо");
        familyController.bornChild(familyDao.getFamilyByIndex(2), "Джеймс", "Кейт");
        familyController.addPet(2, pet3);
        familyController.addPet(2, pet4);

        Family family3 = familyDao.getFamilyByIndex(2);
        families.add(family3);
        familyController.displayAllFamilies();

    }
}

