package HW4_Happy_family;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.Serializable;

public class Human implements Serializable {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Map<DayOfWeek, String> schedule;
    private Family family;
    private Pet pet;
    private Human mother;
    private Human father;

    public Human(String name, String surname, String birthDateStr, int iq, Pet pet, Human mother, Human father, Map<DayOfWeek, String> schedule) {
        this.name = (name != null) ? name : "Undefined name";
        this.surname = surname;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
        this.birthDate = parseBirthDate(birthDateStr);
    }

    public Human(String name, String surname, String birthDateStr) {
        this.name = (name != null) ? name : "Undefined name";
        this.surname = surname;
        this.birthDate = parseBirthDate(birthDateStr);
    }

    public Human(String name, String surname, String birthDateStr, Human mother, Human father) {
        this.name = (name != null) ? name : "Undefined name";
        this.surname = surname;
        this.birthDate = parseBirthDate(birthDateStr);
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, String birthDateStr, int iq) {
        this.name = (name != null) ? name : "Undefined name";
        this.surname = surname;
        this.iq = iq;
        this.birthDate = parseBirthDate(birthDateStr);
    }

    //getters setters
    public String getName() {
        if (name==null){
            return "\"" + "Undefined name" + "\"";
        }
//        return name + " " + surname;
        return this.name + " " + this.surname;

    }

    public long getBirthDate() {
        return birthDate;
    }

   public void setFamily(Family family){
        this.family = family;
   }

    public Family getFamily(){
        return family;
    }

    public Object getPet() {
        return pet;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    public void setDayOfWeek(DayOfWeek day, String activity) {
        schedule.put(day, activity);
    }

    public String getSurname() {
        return  surname;
    };

    //methods

    private long parseBirthDate(String birthDateStr) {
        if (birthDateStr != null) {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date date = dateFormat.parse(birthDateStr);
                return date.getTime();
            } catch (ParseException e) {
                throw new IllegalArgumentException("Invalid birth date format. Use dd/MM/yyyy");
            }
        } else {
            return 0;
        }
    }


    public String describeAge() {
        long currentTime = System.currentTimeMillis();
        long ageInMillis = currentTime - birthDate;

        int days = (int) (ageInMillis / (24 * 60 * 60 * 1000));
        int years = days / 365;
        int months = (days % 365) / 30;
        int remainingDays = days % 30;

        return String.format("%d років, %d місяців і %d днів", years, months, remainingDays);
//        String ageDescription = String.format("%d років, %d місяців і %d днів", years, months, remainingDays);
//        System.out.println("Вік: " + ageDescription);
//        return ageDescription;
    }

    public void greetPet() {

        for(Pet pet: family.getPets()){
            System.out.printf("\nПривіт, %s", pet.getNickname());
        }
    }

    void describePet(){
        System.out.printf("\nУ мене є  %s, йому %d роки, він %s.", pet.getSpecies().getTranslation(), pet.getAge(), pet.getTrickLevel());
    }

    public void feedPet() {
        for (Pet pet: family.getPets()){
            System.out.printf("\nЯ годую %s", pet.getNickname());
        }

    }

    public String prettyFormat() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String birthDateStr = dateFormat.format(new Date(birthDate));
        String scheduleInfo = scheduleToString();

        return String.format("{name='%s', surname='%s', birthDate='%s', iq=%d, schedule=%s}",
                name, surname, birthDateStr, iq, scheduleInfo);
    }

    private String scheduleToString() {
        if (schedule == null) {
            return "null";
        }

        return schedule.entrySet()
                .stream()
                .map(entry -> entry.getKey().name() + ": " + entry.getValue())
                .collect(Collectors.joining(", "));
    }

    @Override
    public String toString() {
        String motherName = (mother != null) ? mother.getName() : "Unknown";
        String fatherName = (father != null) ? father.getName() : "Unknown";
        String petInfo = (pet != null) ? pet.toString() : "No pet";
        String scheduleInfo = (schedule != null) ? scheduleToString() : "No schedule";

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String birthDateStr = dateFormat.format(new Date(birthDate));

        return String.format("Human{name='%s %s', birthDate='%s', iq=%d, mother='%s', father='%s', pet=%s, schedule=%s}",
                name, surname, birthDateStr, iq, motherName, fatherName, petInfo, scheduleInfo);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate);
    }








}

