package HW4_Happy_family;

public final class Man extends Human{
    private int iq;

    public Man(String name, String surname, String birthDateStr) {
        super(name, surname, birthDateStr);
    }

    public Man(String name, String surname, String birthDate, int iq) {
        super(name, surname, birthDate);
        this.iq = iq;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    @Override
    public void greetPet() {
//        Pet pet = getFamily().getPet();

        for(Pet pet: getFamily().getPets()){
            System.out.printf("\nПривіт, %s, друзяко! Пограємось?", pet.getNickname());
        }

    }

    public void repairCar() {
        System.out.println("\nРемонтую авто.");
    }
}
