package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String[][] schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "go to the park";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "go to the cinema";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "go to the gym";
        schedule[5][0] = "Friday";
        schedule[5][1] = "go to a restaurant";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "go to the pool";

        System.out.print("Please, input the day of the week: ");
        Scanner scanner = new Scanner(System.in);
        String userAnswer = scanner.nextLine().trim().toLowerCase();

        while (!userAnswer.equals("exit")) {
            switch (userAnswer) {
                case "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday" ->
                        System.out.println("Your tasks for " + userAnswer + ": " + schedule[getDayIndex(userAnswer)][1]);
                case "change sunday", "change monday", "change tuesday", "change wednesday", "change thursday", "change friday", "change saturday" -> {
                    String[] parts = userAnswer.split(" ");
                    if (parts.length == 3) {
                        String dayToChange = parts[1];
                        int dayIndex = getDayIndex(dayToChange);
                        System.out.print("Please, input new tasks for " + dayToChange + ": ");
                        String newTasks = scanner.nextLine();
                        schedule[dayIndex][1] = newTasks;
                        System.out.println("Tasks for " + dayToChange + " have been updated.");
                    } else {
                        System.out.println("Invalid input. Please use the format: 'change [day of the week]'");
                    }
                }
                default -> System.out.println("Sorry, I don't understand you, please try again.");
            }

            System.out.print("Please, input the day of the week (or 'exit' to quit): ");
            userAnswer = scanner.nextLine().trim().toLowerCase();
        }

        System.out.println("Goodbye!");
    }

    private static int getDayIndex(String day) {
        String[] daysOfWeek = {"sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"};
        for (int i = 0; i < daysOfWeek.length; i++) {
            if (daysOfWeek[i].equals(day)) {
                return i;
            }
        }
        return -1;
    }
}