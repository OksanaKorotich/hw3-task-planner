package HW4_Happy_family;

import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class FamilyServiceTest {
    private FamilyService familyService;
    private Family family1;
    private Family family2;

    @Before
    public void setUp() {
        FamilyDao familyDao = new CollectionFamilyDao();
        familyService = new FamilyService(familyDao);
        family1 = new Family(new Human("John", "Doe", "01/01/1980"), new Human("Jane", "Doe","01/01/1980"));
        family2 = new Family(new Human("Alice", "Smith", "01/01/1985"), new Human("Bob", "Smith", "01/01/1985"));
        familyService.createNewFamily(family1.getFather(), family1.getMother());
        familyService.createNewFamily(family2.getFather(), family2.getMother());
    }

    @Test
    public void testGetAllFamilies() {
        List<Family> allFamilies = familyService.getAllFamilies();
        assertEquals(2, allFamilies.size());
    }

    @Test
    public void testGetFamiliesBiggerThan() {
        List<Family> biggerFamilies = familyService.getFamiliesBiggerThan(2);
        assertEquals(0, biggerFamilies.size());

        biggerFamilies = familyService.getFamiliesBiggerThan(1);
        assertEquals(2, biggerFamilies.size());
    }

    @Test
    public void testGetFamiliesLessThan() {
        List<Family> smallerFamilies = familyService.getFamiliesLessThan(2);
        assertEquals(0, smallerFamilies.size());

        smallerFamilies = familyService.getFamiliesLessThan(3);
        assertEquals(2, smallerFamilies.size());
    }

    @Test
    public void testCountFamiliesWithMemberNumber() {
        long count = familyService.countFamiliesWithMemberNumber(2);
        assertEquals(2, count);

        count = familyService.countFamiliesWithMemberNumber(3);
        assertEquals(0, count);
    }

    @Test
    public void testCreateNewFamily() {
        Human father = new Human("Test", "Father", "01/01/1980");
        Human mother = new Human("Test", "Mother", "01/01/1985");
        familyService.createNewFamily(father, mother);

        List<Family> allFamilies = familyService.getAllFamilies();
        assertEquals(3, allFamilies.size());
    }

    @Test
    public void testDeleteFamilyByIndex() {
        boolean deleted = familyService.deleteFamilyByIndex(0);
        assertTrue(deleted);

        List<Family> allFamilies = familyService.getAllFamilies();
        assertEquals(1, allFamilies.size());
    }

    @Test
    public void testBornChild() {
        Family family = familyService.getFamilyById(0);
        assertNotNull(family);

        int initialChildrenCount = family.getChildren().size();
        familyService.bornChild(family, "Boy", "Girl");
        int newChildrenCount = family.getChildren().size();

        assertEquals(initialChildrenCount + 1, newChildrenCount);
    }

    @Test
    public void testAdoptChild() {
        Family family = familyService.getFamilyById(0);
        assertNotNull(family);

        Human child = new Human("Adopted", "Child", "01/01/2023");
        familyService.adoptChild(family, child);
        int childrenCount = family.getChildren().size();

        assertEquals(1, childrenCount);
    }

    @Test
    public void testDeleteAllChildrenOlderThan() {
        Family family = familyService.getFamilyById(0);
        assertNotNull(family);

        Human olderChild = new Human("Older", "Child", "01/01/2000");
        Human youngerChild = new Human("Younger", "Child", "01/01/2015");
        family.addChild(olderChild);
        family.addChild(youngerChild);
        familyService.deleteAllChildrenOlderThan(15);

        int remainingChildrenCount = family.getChildren().size();
        assertEquals(1, remainingChildrenCount);
    }

    @Test
    public void testCount() {
        int familyCount = familyService.count();
        assertEquals(2, familyCount);
    }


}
