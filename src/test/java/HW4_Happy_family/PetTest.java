package HW4_Happy_family;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;

public class PetTest {
    private Pet pet;

    @Before
    public void setUp(){
        pet = new DomesticCat( "Дрогон", 3, 75, new HashSet<>(Arrays.asList("Flying")));
    }

    @Test
    public void testToString() {
        String expected = "\nДомашній кіт{nickname='Дрогон', age=3, tricklevel=75, habits=[Flying], canFly=false, numberOfLegs=4, hasFur=true}";
        String actual = pet.toString();
        assertEquals(expected, actual);
    }

}
