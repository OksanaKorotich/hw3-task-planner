package HW4_Happy_family;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class HumanTest {
    private Human mother;
    private Human father;
    private Human child;

    @Before
    public void setUp() {
        mother = new Human("Missis", "Smit", "01/01/1988");
        father = new Human("Mister", "Smit", "01/01/1985");
        child = new Human("Adam", "Smit", "01/01/2007", mother, father);
    }

    @Test
    public void testToString() {
        String expected = "Human{name='Adam Smit', birthDate='01/01/2007', iq=0, mother='Missis Smit', father='Mister Smit', pet=No pet, schedule=No schedule}";
        String actual = child.toString();
        assertEquals(expected, actual);
    }
}
