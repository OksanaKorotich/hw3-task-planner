package HW4_Happy_family;

import org.junit.Before;
import org.junit.Test;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class FamilyTest {
    private Human mother;
    private Human father;
    private Family family;



    @Before
    public void setUp() {
        mother = new Human("Missis", "Smit", "01/01/1980");
        father = new Human("Mister", "Smit", "01/01/1979");
        family = new Family(mother, father);
    }

    @Test
    public void testToString() {
        String expected = "Family{Mother: Missis Smit, Father: Mister Smit, No children, No pets}";
        String actual = family.toString();
        assertEquals(expected, actual);
    }

    // перевірте, що дитина дійсно видаляється з масиву children
    // (якщо передати об'єкт, еквівалентний хоча б одному елементу масиву);
    @Test
    public void testDeleteChild() {

        Human child1 = new Human("Lora", "Palmer", "01/01/2005");
        Human child2 = new Human("Adam", "Smit", "01/01/2007");
        Human child3 = new Human("Alex", "Smit", "01/01/2008");

        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);

        boolean result = family.deleteChild(child2);
        assert !Arrays.asList(family.getChildren()).contains(child2);
        assert result;
    }

    @Test
    //перевірте, що масив children залишається без змін
    // (якщо передати об'єкт, не еквівалентний жодному елементу масиву);

    public void testDeleteChildNotInArray() {
        Human child1 = new Human("Lora", "Palmer", "01/01/2005");
        Human child2 = new Human("Adam", "Smit", "01/01/2007");
        Human child3 = new Human("Alex", "Smit", "01/01/2009");

        family.addChild(child3);
        family.addChild(child2);

        boolean result = family.deleteChild(child1);
        assertFalse(result);
        assertFalse(Arrays.asList(family.getChildren()).contains(child1));
    }


    //Перевірте, що дитина дійсно видаляється з масиву children і метод повертає правильне значення;

    @Test
    public void testDeleteChildByIndex() {
        Human child1 = new Human("Lora", "Palmer", "01/01/2005");
        Human child2 = new Human("Adam", "Smit", "01/01/2007");
        Human child3 = new Human("Alex", "Smit", "01/01/2009");

        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);

        boolean result = family.deleteChild(1); // Видалення другої дитини (індекс 1)
        assert !Arrays.asList(family.getChildren()).contains(child2);
        assert result;
    }

    // перевірте, що масив children залишається без змін
    // (якщо передати індекс, що виходить за діапазон індексів), та метод повертає правильне значення;
    @Test
    public void testDeleteChildByInvalidIndex() {
        Human child1 = new Human("Lora", "Palmer", "01/01/2005");
        Human child2 = new Human("Adam", "Smit", "01/01/2007");
        Human child3 = new Human("Alex", "Smit", "01/01/2009");

        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);

        boolean result = family.deleteChild(3);
        assert !result;
    }
    @Test
    public void testAddChild() {
        Human child2 = new Human("Adam", "Smit", "01/01/2007");
        family.addChild(child2);
        List<Human> children = family.getChildren();
        assertEquals(1, children.size());
        assertSame(child2, children.get(0));
    }

    @Test
    public void testCountFamily() {
        assertEquals(2, family.countFamily());
        Human child2 = new Human("Adam", "Smit", "01/01/2007");
        family.addChild(child2);
        assertEquals(3, family.countFamily());
    }

}
